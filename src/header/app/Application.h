#ifndef APPLICATION_H
#define APPLICATION_H

#include <window/Input.h>
#include <window/Window.h>
#include <vulkansetup/Renderer.h>

class Application: vulkansetup::Renderer {
private:
	ScopePtr<Window> m_window;
	Application(std::string&& title = "Shae", bool resizable = true,
		unsigned int width = 1600, unsigned int height = 900, WindowMode mode = WindowMode::Windowed, bool vsync = false);
	Application(WindowData&&, bool vsync = false);
	void KeyButtonCallback(int key, int action);
	void MouseButtonCallback(int key, int action);
	void MousePosCallback(double x, double y);
	void ScrollCallback();
	void FrameBufferResizeCallback(int width, int height);
	void Init();
	
public:
	Input input;
	Application(const Application &) = delete;
	Application(Application &&) = delete;

	~Application() = default;

	Application &operator=(const Application &) = delete;
	Application &operator=(Application &&) = delete;
	static Application* CreateApplication();
	static void Initialize();
	static void Shutdown();
	bool Running();
};
#endif