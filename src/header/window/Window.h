#ifndef WINDOW_H
#define WINDOW_H
#include <GLFW/glfw3.h>
#include <string>

enum class WindowMode {
	Windowed,
	Fullscreen
};

struct WindowData {
	//Title of the window.
	std::string winTitle;
	//Resizability flag
	bool resizable;
	//Width and height of the window.
	unsigned int width, height;
	WindowMode winMode;
	//To be added: Camera/view matrix & Projection matrix
	WindowData(std::string&& title, bool resizable, unsigned int width, unsigned int height, WindowMode mode);
	WindowData(WindowData&&);
};

class Application;

class Window {
	friend class Application;
public:
	void Resize(unsigned int width, unsigned int height);
	void SetVSync(bool);
	Window(const Window&) = delete;
	Window(Window &&) = delete;

	Window &operator=(const Window &) = delete;
	Window &operator=(Window &&) = delete;
	~Window();
	static Window* CreateNewWindow(WindowData&&, bool vsync);
private:
	static void Init();
	static void Shutdown();
	bool IsRunning();
	void Draw();
	void Update();
	void SetUserPtr(void*);
	void SetFrameBufferCallback(GLFWframebuffersizefun);
	void SetKeyButtonCallback(GLFWkeyfun);
	void SetMouseButtonCallback(GLFWmousebuttonfun);
	void SetScrollCallback(GLFWscrollfun);
	void SetCursorPosCallback(GLFWcursorposfun);
	//Must be called before creating any window to create context. Called by Application::Init()
	Window(WindowData&&, bool);
	void Init(bool);
	inline GLFWwindow* GetWindow() const {
		return m_window;
	}
	inline unsigned int GetWidth() {
		return m_winData.width;
	}
	inline unsigned int GetHeight() {
		return m_winData.height;
	}
	WindowData m_winData;
	GLFWwindow* m_window;
};
#endif