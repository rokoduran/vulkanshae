#ifndef INPUT_H
#define INPUT_H

#include <glm/glm.hpp>
#include <GLFW/glfw3.h>

#define MAX_KEYBOARD_KEYS  1024
#define MAX_MOUSE_KEYS     32

enum KeyButtons {
	KEYBOARD_UP = GLFW_KEY_UP,
	KEYBOARD_LEFT = GLFW_KEY_LEFT,
	KEYBOARD_RIGHT = GLFW_KEY_RIGHT,
	KEYBOARD_DOWN = GLFW_KEY_DOWN,
	KEYBOARD_ESC = GLFW_KEY_ESCAPE,
	KEYBOARD_ENTER = GLFW_KEY_ENTER,
	KEYBOARD_BACKSPACE = GLFW_KEY_BACKSPACE,
	KEYBOARD_SPACE = GLFW_KEY_SPACE,
	KEYBOARD_CAPS = GLFW_KEY_CAPS_LOCK,
	KEYBOARD_1 = GLFW_KEY_1,
	KEYBOARD_2 = GLFW_KEY_2,
	KEYBOARD_3 = GLFW_KEY_3,
	KEYBOARD_4 = GLFW_KEY_4,
	KEYBOARD_5 = GLFW_KEY_5,
	KEYBOARD_F1 = GLFW_KEY_F1,
	KEYBOARD_F2 = GLFW_KEY_F2,
	KEYBOARD_F3 = GLFW_KEY_F3,
	KEYBOARD_F4 = GLFW_KEY_F4,
	KEYBOARD_F5 = GLFW_KEY_F5,
	LEFT_CLICK = GLFW_MOUSE_BUTTON_LEFT,
	RIGHT_CLICK = GLFW_MOUSE_BUTTON_RIGHT,
	MIDDLE_CLICK = GLFW_MOUSE_BUTTON_MIDDLE
};

class Application;

class Input {
	friend class Application;
private:
	Input() = default;
	Input(const Input &) = delete;
	Input(Input &&) = delete;

	~Input() = default;

	Input &operator=(const Input &) = delete;
	Input &operator=(Input &&) = delete;

	void SetScrollValue(float);
	bool m_pressedKeyboardKeys[MAX_KEYBOARD_KEYS];
	bool m_repeatedKeyboardKeys[MAX_KEYBOARD_KEYS];
	bool m_mouseKeys[MAX_MOUSE_KEYS];
	glm::vec2 m_currentMousePos = { 0.0f,0.0f };
	glm::vec2 m_previousMousePos = { 0.0f,0.0f };
	float m_scrollValue = 0.0f;
public:
	bool IsKeyPressed(KeyButtons);
	bool IsKeyHeld(KeyButtons);
	bool IsMouseButtonPressed(KeyButtons);
	glm::vec2 CalcMouseDelta();
	glm::vec2& GetMousePos();
};
#endif