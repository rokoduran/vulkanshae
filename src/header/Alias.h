#ifndef ALIAS_H
#define ALIAS_H
#include <string>
#include <memory>
#include <map>
#include <unordered_map>
#include <list>

template<class T, class Y>
using Map = std::map<T, Y>;

template<class T, class Y>
using UnorderedMap = std::unordered_map<T, Y>;

template<class T, class Y>
using Pair = std::pair<T, Y>;

template<class T>
using List = std::list<T>;

using String = std::string;

template <class T>
using ScopePtr = std::unique_ptr<T>;

template<class T>
using RefPtr = std::shared_ptr<T>;

template<class T>
using WeakPtr = std::weak_ptr<T>;

#endif