#ifndef DEVICE_H
#define DEVICE_H

#include <vulkan/vulkan_core.h>
#include <vma/vk_mem_alloc.h>
#include <string>

namespace vulkansetup {

	struct QueueFamilyIndices {
		uint32_t graphicsFamily;
		uint32_t presentationFamily;
		bool validGraphics = false;
		bool validPresentation = false;
	};

	class Device {
	public:
		Device(VkInstance instance, VkSurfaceKHR surface, bool renderDocEnabled);
		Device(const Device &) = delete;
		Device(Device &&) = delete;
		//Below methods are for naming and allocating vulkan objects with VMA and allowing them to be visible and interactable in RenderDoc
		//Set debug name for vulkan object
		void SetDebugMarkerName(void *object, VkDebugReportObjectTypeEXT objectType, const std::string &name) const;
		//Assign memory block to vulkan object/resource
		void AssignMemoryBlock(void *object, VkDebugReportObjectTypeEXT objectType, 
			uint64_t name,size_t memorySize, const void *memoryBlock) const;
		//Bind debug region with colors (use SwapHex function from utilities for creating the color!)
		void BindDebugRegion(VkCommandBuffer commandBuffer, const std::string &name, unsigned int color) const;
		//Insert debug marker into the current render pass
		void InsertDebugMarker(VkCommandBuffer commandBuffer, const std::string &name, unsigned int color) const;
		//End the debug region of the current render pass.
		void EndDebugRegion(VkCommandBuffer commandBuffer) const;
		bool IsExtensionSupported(const VkPhysicalDevice& gcard, const std::string& extension)const;
		bool IsLayerSupported(const VkPhysicalDevice& gcard, const std::string& layer)const;
		bool IsSurfaceSupported(const VkPhysicalDevice& gcard, VkSurfaceKHR surface)const;
		
		inline VkDevice GetLogicDevice() const {
			return m_device;
		}
		inline VmaAllocator GetAllocator()const{
			return m_allocator;
		}
		inline VkPhysicalDevice GetPhysicalDevice() const {
			return m_graphicsCard;
		}
		inline const std::string& GetGPUName()const {
			return m_gpuName;
		}
		inline VkQueue GetPresentationQueue()const{
			return m_presentQueue;
		};
		inline VkQueue GetGraphicsQueue()const{
			return m_graphicsQueue;
		};		
		inline VkQueue GetTransferQueue()const{
			return m_transferQueue;
		};
		inline uint32_t GetGraphicsFamilyIndex() const {
			return m_graphicsFamilyIndex;
		}
		inline uint32_t GetPresentationFamilyIndex() const {
			return m_presentFamilyIndex;
		}
		inline uint32_t GetTransferFamilyIndex()const{
			return m_transferFamilyIndex;
		}
		~Device();

		Device &operator=(const Device &) = delete;
		Device &operator=(Device &&) = delete;
	private:
		QueueFamilyIndices FindQueueFamilies(VkPhysicalDevice& device);
		VkPhysicalDevice ChooseGraphicsCard(const VkInstance& instance);
		bool IsCardCompatible(const VkPhysicalDevice& graphicsCard);
		void Init(const VkInstance& instance);

		VkDevice m_device;
		VkPhysicalDevice m_graphicsCard = VK_NULL_HANDLE;

		VmaAllocator m_allocator;
		std::string m_gpuName;

		VkQueue m_graphicsQueue = nullptr;
		VkQueue m_presentQueue = nullptr;
		VkQueue m_transferQueue = nullptr;
		VkSurfaceKHR m_surface;

		uint32_t m_graphicsFamilyIndex;
		uint32_t m_presentFamilyIndex;
		uint32_t m_transferFamilyIndex;

		//Debug mode only. Used for RenderDoc.
		PFN_vkDebugMarkerSetObjectTagEXT m_vkDebugMarkerSetObjectTag;
		PFN_vkDebugMarkerSetObjectNameEXT m_vkDebugMarkerSetObjectName;
		PFN_vkCmdDebugMarkerBeginEXT m_vkCmdDebugMarkerBegin;
		PFN_vkCmdDebugMarkerEndEXT m_vkCmdDebugMarkerEnd;
		PFN_vkCmdDebugMarkerInsertEXT m_vkCmdDebugMarkerInsert;
		PFN_vkSetDebugUtilsObjectNameEXT m_vkSetDebugUtilsObjectName;

		bool m_renderDocEnabled;
	};
}
#endif