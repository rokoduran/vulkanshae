#ifndef FRAMEBUFFER_H
#define FRAMEBUFFER_H
#include <vulkan/vulkan_core.h>
#include <string>
#include <vector>

namespace vulkansetup{
    class Device;

    class Framebuffer{
    public:
        Framebuffer(const Device& device, VkRenderPass renderPass, VkExtent2D extent, const std::vector<VkImageView>& viewAttachments, const std::string& debugName);

        ~Framebuffer();

        VkFramebuffer inline GetFramebuffer()const{
            return m_framebuffer;
        }
    private:
        const Device &m_device;
        VkFramebuffer m_framebuffer = VK_NULL_HANDLE;
        std::string m_debugName;
    };
}

#endif