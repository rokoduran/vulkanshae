#ifndef BASEGPUBUFFER_H
#define BASEGPUBUFFER_H

#include <vulkan/vulkan_core.h>
#include <vma/vk_mem_alloc.h>
#include <string>

namespace vulkansetup{
    class Device;
    class BaseGPUBuffer{
    public:
    //Construct GPU Buffer without data to fill.
        BaseGPUBuffer(const Device& device, const std::string& debugName, const VkDeviceSize& size,
        const VkBufferUsageFlags& bufferUsage, const VmaMemoryUsage& memoryUsage);
    //Construct GPU buffer with data and it's size
        BaseGPUBuffer(const Device& device, const std::string& debugName, const VkDeviceSize& size, void* dataToFill, size_t dataSize,
        const VkBufferUsageFlags& bufferUsage, const VmaMemoryUsage& memoryUsage);

        BaseGPUBuffer(BaseGPUBuffer&&) noexcept;

        virtual ~BaseGPUBuffer();

        BaseGPUBuffer &operator=(const BaseGPUBuffer &) = delete;
        BaseGPUBuffer &operator=(BaseGPUBuffer &&) = delete;

        inline VkBuffer GetBuffer()const{
            return m_buffer;
        }
        inline const std::string& GetName()const{
            return m_debugName;
        }
        inline VmaAllocation GetAllocation()const{
            return m_allocation;
        }
        inline VmaAllocationInfo GetAllocationInfo()const{
            return m_allocationInfo;
        }
        inline VmaAllocationCreateInfo GetCreateInfo()const{
            return m_allocCreateInfo;
        }

    protected:
        const Device& m_device;
        std::string m_debugName;
        VkBuffer m_buffer = VK_NULL_HANDLE;
        VkDeviceSize m_bufferSize = 0;
        VmaAllocation m_allocation = VK_NULL_HANDLE;
        VmaAllocationInfo m_allocationInfo = {};
        VmaAllocationCreateInfo m_allocCreateInfo= {};
    };
}

#endif