#ifndef VERTEXINDEXBUFFER_H
#define VERTEXINDEXBUFFER_H
#include <vulkansetup/BaseGPUBuffer.h>

namespace vulkansetup{
    class VertexIndexBuffer: public BaseGPUBuffer{
    public:
        VertexIndexBuffer(const Device& device, const std::string& debugName, const VkDeviceSize& size);
        void SubmitVertices(void* data, size_t dataSize);
        void SubmitIndices(void* data, size_t dataSize);
    private:

    }
}
#endif