#ifndef DEBUG_REPORT_H
#define DEBUG_REPORT_H
#include <vulkan/vulkan_core.h>

namespace vulkansetup {

class DebugReport {
public:
	static VKAPI_ATTR VkBool32 VKAPI_CALL DebugCallback(VkDebugUtilsMessageSeverityFlagBitsEXT messageSeverity, VkDebugUtilsMessageTypeFlagsEXT messageType, const VkDebugUtilsMessengerCallbackDataEXT* pCallbackData, void* pUserData);
	VkResult CreateDebugUtilsMessengerEXT(VkInstance& instance, const VkDebugUtilsMessengerCreateInfoEXT* pCreateInfo, const VkAllocationCallbacks* pAllocator, VkDebugUtilsMessengerEXT* pDebugMessenger);
	void PopulateDebugMessengerCreateInfo(VkDebugUtilsMessengerCreateInfoEXT& createInfo);
	DebugReport(VkInstance instance);
	void DestroyDebugUtilsMessengerEXT(const VkAllocationCallbacks* pAllocator, VkInstance instance);
private:
	VkDebugUtilsMessengerEXT m_debugMessenger;
};

}
#endif