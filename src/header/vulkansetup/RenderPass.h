#ifndef RENDERPASS_H
#define RENDERPASS_H
#include <vulkan/vulkan_core.h>
#include <vector>
#include <string>


namespace vulkansetup{
    class Device;
    class RenderPass{
    public:
        /*Create render pass for device. Takes in vector of attachment description & subpass dependencies as this 
        depends on swapchain color format.*/
        RenderPass(const Device& device, const std::string& debugName, const std::vector<VkAttachmentDescription>& attachmentDescripts,
         const std::vector<VkSubpassDependency>& dependencies, VkSubpassDescription subpassDescription);
        ~RenderPass();

        RenderPass(const RenderPass &) = delete;
        RenderPass(RenderPass &&other) = delete;
        RenderPass &operator=(const RenderPass &) = delete;
        RenderPass &operator=(RenderPass &&) = delete;
    private:
        VkRenderPass m_renderPass;
        const Device& m_device;
        std::string m_debugName;
    };
}

#endif