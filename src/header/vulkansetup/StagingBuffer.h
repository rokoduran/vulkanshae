#ifndef STAGINGBUFFER_H
#define STAGINGBUFFER_H
#include <vulkan/vulkan_core.h>
#include <vulkansetup/BaseGPUBuffer.h>
#include <vulkansetup/SingleUseCommandBuffer.h>

namespace vulkansetup{
    class Device;
    class StagingBuffer: public BaseGPUBuffer{
    public:
        StagingBuffer(const Device& device, const std::string& debugName, VkDeviceSize bufferSize, void* dataToFill, size_t dataSize);
        void TransferDataToBuffer(BaseGPUBuffer& destination, VkDeviceSize sourceOffset, VkDeviceSize destinationOffset);

        StagingBuffer(const StagingBuffer &) = delete;
        StagingBuffer(StagingBuffer &&) = delete;
        StagingBuffer &operator=(const StagingBuffer &) = delete;
        StagingBuffer &operator=(StagingBuffer &&) = delete;
    private:
        SingleUseCommandBuffer m_singleUseCmdBuffer;
    };
}
#endif