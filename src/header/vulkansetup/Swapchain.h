#ifndef SWAPCHAIN_H
#define SWAPCHAIN_H
#include <vulkan/vulkan_core.h>
#include <vector>
#include <string>

namespace vulkansetup {

    class Device;
    //class Semaphore;

    class Swapchain{
    public:
        Swapchain(const Device& device, VkSurfaceKHR surface, uint32_t windowWidth, uint32_t windowHeight, const std::string& name, bool vsync);
        ~Swapchain();

        Swapchain(const Swapchain &) = delete;
        Swapchain(Swapchain &&) = delete;
        Swapchain &operator=(const Swapchain &) = delete;
        Swapchain &operator=(Swapchain &&) = delete;

        inline VkSwapchainKHR GetSwapchain()const{
            return m_swapchain;
        }
        inline uint32_t GetSwapchainImageCount()const{
            return m_swapchainImageCount;
        }
        inline VkSurfaceFormatKHR GetSurfaceFormat()const{
            return m_surfaceFormat;
        }
        inline VkExtent2D GetExtent()const{
            return m_extent;
        }
        inline VkImageView GetImageViewAtIndex(uint32_t index)const;

        void ToggleVsync(bool vsync);
        //Recreate the swapchain after framebuffer resizing.
        void RecreateSwapchain(uint32_t windowWidth, uint32_t windowHeight);
    private:
        /*Setup swapchain. Called ater framebuffer resizing with the old swapchain and new window dimensions.
        Initially called when creating a swapchain with NULL HANDLE for old swapchain.
        */
        void CreateSwapchain(VkSwapchainKHR oldSwapchain, uint32_t windowWidth, uint32_t windowHeight);
        VkSurfaceTransformFlagBitsKHR GetSurfaceCapabilitiesData(uint32_t& windowWidth, uint32_t& windowHeight);
        VkPresentModeKHR ChoosePresentationMode();
        void ChooseSurfaceColorFormat();
        void CreateImageViews();
        VkSurfaceKHR m_surface;
        VkSwapchainKHR m_swapchain;
        VkSurfaceFormatKHR m_surfaceFormat;
        VkExtent2D m_extent;
        const Device& m_device;
        std::vector<VkImage> m_swapchainImages;
        std::vector<VkImageView> m_swapchainImageViews;
        uint32_t m_swapchainImageCount;
        std::string m_debugName;
        bool m_vsync;
    };
}
#endif