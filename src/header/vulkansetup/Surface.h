#ifndef SURFACE_H
#define SURFACE_H

#define GLFW_INCLUDE_VULKAN
#include <GLFW/glfw3.h>

namespace vulkansetup {
class Surface{
public:
	Surface(VkInstance instance, GLFWwindow* window);
	~Surface();
	Surface(Surface &&) = delete;
	Surface(const Surface &) = delete;


	Surface &operator=(const Surface &) = delete;
	Surface &operator=(Surface &&) = delete;

	inline VkSurfaceKHR GetSurface() const{
		return m_surface;
	}
private:
	VkInstance m_instance;
	VkSurfaceKHR m_surface;
	};
}
#endif