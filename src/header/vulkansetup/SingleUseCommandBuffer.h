#ifndef SINGLEUSECOMMANDBUFFER_H
#define SINGLEUSECOMMANDBUFFER_H
#include <vulkan/vulkan_core.h>
#include <vulkansetup/CommandPool.h>
#include <string>

namespace vulkansetup{
    class Device;
    class CommandPool;
    
    class SingleUseCommandBuffer{
    public:
        SingleUseCommandBuffer(const Device& device);
        void CreateCommandBuffer();
        void BeginRecording();
        void EndRecordingAndSubmit();
        ~SingleUseCommandBuffer();

        SingleUseCommandBuffer(const SingleUseCommandBuffer &) = delete;
        SingleUseCommandBuffer(SingleUseCommandBuffer &&) = delete;
        SingleUseCommandBuffer &operator=(const SingleUseCommandBuffer &) = delete;
        SingleUseCommandBuffer &operator=(SingleUseCommandBuffer &&) = delete;
        inline VkCommandBuffer GetCommandBuffer(){
            return m_singleUseCommandBuffer;
        }
    private:
        CommandPool m_commandPool;
        const Device& m_device;
        VkQueue m_queue;
        VkCommandBuffer m_singleUseCommandBuffer = VK_NULL_HANDLE;
        bool m_recordingBegan;
    };
}
#endif