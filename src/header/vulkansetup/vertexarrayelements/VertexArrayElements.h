#ifndef VERTEXARRAYELEMENTS_H
#define VERTEXARRAYELEMENTS_H

#include <glm/glm.hpp>
#include <vulkan/vulkan_core.h>
#include <array>

struct VertexArrayElements {
	glm::vec3 position;
	unsigned int color;
	glm::vec2 texCoord;

	static const VkVertexInputBindingDescription GetBindingDescription(){ 
		VkVertexInputBindingDescription bindingDescription = { 0, sizeof(VertexArrayElements),VK_VERTEX_INPUT_RATE_VERTEX };
		return bindingDescription;
	};
	static const std::array<VkVertexInputAttributeDescription,3> GetAttributeDescriptions() { 
		std::array<VkVertexInputAttributeDescription, 3> attributeDescriptions{};
		attributeDescriptions[0] = { 0,0, VK_FORMAT_R32G32B32_SFLOAT, offsetof(VertexArrayElements, position) };
		attributeDescriptions[1] = { 1,0, VK_FORMAT_R8G8B8A8_UNORM, offsetof(VertexArrayElements, color) };
		attributeDescriptions[2] = { 2,0, VK_FORMAT_R32G32_SFLOAT, offsetof(VertexArrayElements, texCoord) };
		return attributeDescriptions;
	};
};
#endif