#ifndef RENDERER_H
#define RENDERER_H

#include <Alias.h>
#include <vulkansetup/Instance.h>
#include <vulkansetup/Surface.h>
#include <vulkansetup/DebugReport.h>
#include <vulkansetup/Device.h>
#include <vulkansetup/Swapchain.h>

namespace vulkansetup {
	class Renderer {
	protected:
		ScopePtr<Instance> m_instance = nullptr;
		ScopePtr<Surface> m_surface = nullptr;
		ScopePtr<DebugReport> m_debugReport = nullptr;
		ScopePtr<Device> m_device = nullptr;
		ScopePtr<Swapchain> m_swapchain = nullptr;
	public:
		Renderer() = default;
		Renderer(const Renderer &) = delete;
		Renderer(Renderer &&) = delete;
		~Renderer() {
			if (m_debugReport != nullptr) {
				m_debugReport->DestroyDebugUtilsMessengerEXT(nullptr, m_instance->GetInstance());
			}
		};

		Renderer &operator=(const Renderer &) = delete;
		Renderer &operator=(Renderer &&) = delete;
	};
}
#endif