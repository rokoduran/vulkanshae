#ifndef INSTANCE_H
#define INSTANCE_H

#include <vulkan/vulkan_core.h>
#include <string>
#include <vector>

namespace vulkansetup {

class Instance {
public:
	bool IsValidationLayerSupported(const std::string& layerName);
	bool IsExtensionSupported(const std::string& extName);

	Instance(const std::string& appName, uint32_t appVersion, uint32_t vulkanApiVersion, bool enableValidLayers, bool& enableRenderDoc,
		std::vector<std::string> requiredExtensions, std::vector<std::string> requestedLayers);

	inline VkInstance GetInstance() const {
		return m_instance;
	};

	~Instance();

private:
	VkInstance m_instance = VK_NULL_HANDLE;
};

}
#endif