#ifndef COMMANDPOOL_H
#define COMMANDPOOL_H
#include <vulkan/vulkan_core.h>

namespace vulkansetup{
    class Device;
    class CommandPool{
    public:
        //Create a command pool for the logical device. Family index can be either the family,presentation
        CommandPool(const Device& device, VkCommandPoolCreateFlagBits flags);
        ~CommandPool();
        CommandPool(const CommandPool &) = delete;
        CommandPool(CommandPool &&) = delete;

        CommandPool &operator=(const CommandPool &) = delete;
        CommandPool &operator=(CommandPool &&) = delete;
        inline VkCommandPool& GetCommandPool(){
            return m_commandPool;
        }
    private:
        const Device& m_device;
        VkCommandPool m_commandPool = VK_NULL_HANDLE;
    };
}
#endif