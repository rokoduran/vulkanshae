#ifndef FILEMANIPULATION_H
#define FILEMANIPULATION_H
#include <Alias.h>
#include <vector>

namespace FileManipulation{
	String ReadFileToStr(String& path);
	String ReadFileToStr(String path);
	std::vector<char> ReadFileToVec(String& path);
}
#endif