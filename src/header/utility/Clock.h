#ifndef CLOCK_H
#define CLOCK_H

#include <chrono>

class Clock {
private:
	std::chrono::system_clock::time_point m_start;
public:
	Clock() {
		Start();
	};

	// Returns the elapsed time and starts measuring time from 0
	double Restart() {
		double res = Elapsed().count();
		Start();
		return res;
	};

	// Returns the amount of seconds passed since last restart or construction if the clock was never restarted
	// Returns a std::chrono::duration<double>
	// Use get_seconds() to get the amount of elapsed seconds

	// Returns the precise amount of seconds passed since last restart
	double GetSeconds() const {
		return Elapsed().count();
	}

private:
	// Used internally by the constructor and restart()
	inline void Start() {
		m_start = std::chrono::system_clock::now();
	};

	std::chrono::duration<double> Elapsed() const {
		std::chrono::duration<double> res;
		auto elapsed = std::chrono::system_clock::now();

		res = elapsed - m_start;

		return res;
	};
};
#endif