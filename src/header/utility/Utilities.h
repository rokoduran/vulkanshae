#ifndef UTILITIES_H
#define UTILITIES_H
#include <array>

//Swap recieved hex values to match the wanted color
static void SwapHexByRef(unsigned int& color) {
	color = ((color << 24) & 0xFF000000) |
		((color << 8) & 0x00FF0000) |
		((color >> 8) & 0x0000FF00) |
		((color >> 24) & 0x000000FF);
};
//Swap recieved hex values to match the wanted color
static unsigned int SwapHex(unsigned int color) {
	color = ((color << 24) & 0xFF000000) |
		((color << 8) & 0x00FF0000) |
		((color >> 8) & 0x0000FF00) |
		((color >> 24) & 0x000000FF);
	return color;
};

static std::array<float, 4> SwapUnsignedIntToFloatArray(unsigned int color) {
	float a = (float)(color >> 24 & 255)/255.0f; 
	float r = (float)(color >> 16 & 255)/255.0f; 
	float g = (float)(color >> 8 & 255)/255.0f;
	float b = (float)(color >> 0 & 255)/255.0f;

	std::array<float, 4> arr{ r,g,b,a };
	return arr;
}
#endif