#ifndef LOG_H
#define LOG_H
#include <spdlog/spdlog.h>
#include <Alias.h>

class Log {
public:
	static void Init();
	inline static RefPtr<spdlog::logger>& GetCoreLogger() { return s_coreLogger; }
	inline static RefPtr<spdlog::logger>& GetAppLogger() { return s_appLogger; }
private:
	static RefPtr<spdlog::logger> s_coreLogger;
	static RefPtr<spdlog::logger> s_appLogger;
};

//Macros for ease of use
#ifdef DEBUG
#define CORE_TRACE(...) Log::GetCoreLogger()->trace(__VA_ARGS__)
#define CORE_WARN(...)  Log::GetCoreLogger()->warn(__VA_ARGS__)
#define CORE_INFO(...)  Log::GetCoreLogger()->info(__VA_ARGS__)
#define CORE_ERROR(...) Log::GetCoreLogger()->error(__VA_ARGS__)
// assert(0)
#define APP_TRACE(...)  Log::GetAppLogger()->trace(__VA_ARGS__)
#define APP_WARN(...)   Log::GetAppLogger()->warn(__VA_ARGS__)
#define APP_INFO(...)   Log::GetAppLogger()->info(__VA_ARGS__)
#define APP_ERROR(...)  Log::GetAppLogger()->error(__VA_ARGS__)
#else
#define CORE_TRACE(...)
#define CORE_WARN(...) 
#define CORE_INFO(...) 
#define CORE_ERROR(...)
#define APP_TRACE(...) 
#define APP_WARN(...)  
#define APP_INFO(...)  
#define APP_ERROR(...)
#endif

#endif