#include <utility/logging/Log.h>
#include <spdlog/sinks/stdout_color_sinks.h>
#include <Alias.h>

RefPtr<spdlog::logger> Log::s_coreLogger;
RefPtr<spdlog::logger> Log::s_appLogger;

void Log::Init() {
	//Timestamp - logger name - message
	spdlog::set_pattern("%^[%T] %n: %v%$");
	s_coreLogger = spdlog::stdout_color_mt("SHAE");
	s_coreLogger->set_level(spdlog::level::trace);
	s_appLogger = spdlog::stdout_color_mt("APP");
	s_coreLogger->set_level(spdlog::level::trace);
}