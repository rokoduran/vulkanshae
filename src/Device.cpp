#include <vulkansetup/Device.h>

#define VMA_IMPLEMENTATION

#ifdef DEBUG
#define VMA_DEBUG_INITIALIZE_ALLOCATIONS 1

//Enforced offset between allocations
#define VMA_DEBUG_MARGIN 8

#define VMA_DEBUG_DETECT_CORRUPTION 1
#endif

#include "../utility/logging/Log.h"
#include "../utility/Utilities.h"
#include <set>
#include <vma/VmaUsage.h>

namespace vulkansetup {
	Device::Device(VkInstance instance, VkSurfaceKHR surface, bool renderDocEnabled) : m_surface{ surface },
		m_renderDocEnabled{renderDocEnabled} {

		Init(instance);
	};

void Device::Init(const VkInstance& instance) {
	m_graphicsCard = ChooseGraphicsCard(instance);
	CORE_INFO("[Vulkan Info] Creating logical device...");
	VkPhysicalDeviceProperties graphicsCardProperties;
	vkGetPhysicalDeviceProperties(m_graphicsCard, &graphicsCardProperties);
	m_gpuName = graphicsCardProperties.deviceName;
	CORE_INFO("[Vulkan Info] The graphics card {} chosen for the creation of the device", graphicsCardProperties.deviceName);

	CORE_INFO("[Vulkan Info] Finding queue families for graphics queues...");

	QueueFamilyIndices indices = FindQueueFamilies(m_graphicsCard);

	std::vector<VkDeviceQueueCreateInfo> queueCreateInfos;
	std::set<uint32_t> uniqueQueueFamilies = { indices.graphicsFamily, indices.presentationFamily};

	m_graphicsFamilyIndex = indices.graphicsFamily;
	m_presentFamilyIndex = indices.presentationFamily;
	m_transferFamilyIndex = indices.graphicsFamily;

	float queuePriority = 1.0f;
	for (uint32_t queueFamily : uniqueQueueFamilies) {
		VkDeviceQueueCreateInfo queueCreateInfo{};
		queueCreateInfo.sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO;
		queueCreateInfo.queueFamilyIndex = indices.graphicsFamily;
		queueCreateInfo.queueCount = 1;
		queueCreateInfo.pQueuePriorities = &queuePriority;
		queueCreateInfos.push_back(queueCreateInfo);
	}
	std::vector<const char*> deviceExtensions = {
		VK_KHR_SWAPCHAIN_EXTENSION_NAME,
	};

#ifdef DEBUG
	if (m_renderDocEnabled) {
		//RenderDoc debug marker extension
		deviceExtensions.push_back(VK_EXT_DEBUG_MARKER_EXTENSION_NAME);
	}
#endif
	for (auto& i : deviceExtensions) {
		if (!IsExtensionSupported(m_graphicsCard, i)) {
			CORE_ERROR("[Vulkan Error] Device extension {} is unavailable!", i);
		}
	}


	VkPhysicalDeviceFeatures deviceFeatures{};
	deviceFeatures.samplerAnisotropy = VK_TRUE;

	VkDeviceCreateInfo createInfo{};
	createInfo.sType = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO;

	createInfo.queueCreateInfoCount = static_cast<uint32_t>(queueCreateInfos.size());
	createInfo.pQueueCreateInfos = queueCreateInfos.data();

	createInfo.queueCreateInfoCount = 1;

	createInfo.pEnabledFeatures = &deviceFeatures;

	createInfo.enabledExtensionCount = static_cast<uint32_t>(deviceExtensions.size());
	createInfo.ppEnabledExtensionNames = deviceExtensions.data();
	//Device level validation layers are deprecated, so it's 0
	createInfo.enabledLayerCount = 0;

	if (vkCreateDevice(m_graphicsCard, &createInfo, nullptr, &m_device) != VK_SUCCESS) {
		CORE_ERROR("[Vulkan Error] Failed to create logical device!");
	}
#ifdef DEBUG
	if (m_renderDocEnabled) {
		CORE_INFO("[Vulkan Info] RenderDoc is enabled. Fetching function pointers & creating debug markers");
		m_vkDebugMarkerSetObjectTag = reinterpret_cast<PFN_vkDebugMarkerSetObjectTagEXT>(
			vkGetDeviceProcAddr(m_device, "vkDebugMarkerSetObjectTagEXT"));

		m_vkDebugMarkerSetObjectName = reinterpret_cast<PFN_vkDebugMarkerSetObjectNameEXT>(
			vkGetDeviceProcAddr(m_device, "vkDebugMarkerSetObjectNameEXT"));

		m_vkCmdDebugMarkerBegin =
			reinterpret_cast<PFN_vkCmdDebugMarkerBeginEXT>(vkGetDeviceProcAddr(m_device, "vkCmdDebugMarkerBeginEXT"));

		m_vkCmdDebugMarkerEnd =
			reinterpret_cast<PFN_vkCmdDebugMarkerEndEXT>(vkGetDeviceProcAddr(m_device, "vkCmdDebugMarkerEndEXT"));

		m_vkCmdDebugMarkerInsert =
			reinterpret_cast<PFN_vkCmdDebugMarkerInsertEXT>(vkGetDeviceProcAddr(m_device, "vkCmdDebugMarkerInsertEXT"));

		m_vkSetDebugUtilsObjectName = reinterpret_cast<PFN_vkSetDebugUtilsObjectNameEXT>(
			vkGetDeviceProcAddr(m_device, "vkSetDebugUtilsObjectNameEXT"));
	}
#endif
	vkGetDeviceQueue(m_device, m_graphicsFamilyIndex, 0, &m_graphicsQueue);
	vkGetDeviceQueue(m_device, m_presentFamilyIndex, 0, &m_presentQueue);
	vkGetDeviceQueue(m_device, m_transferFamilyIndex,0, &m_transferQueue);

	CORE_INFO("[Vulkan Info] Device queues initialized.");

	CORE_INFO("[Vulkan Info] Creating VMA allocator...");

	VmaAllocatorCreateInfo vmaAllocatorCi = {};
	vmaAllocatorCi.physicalDevice = m_graphicsCard;
	vmaAllocatorCi.instance = instance;
	vmaAllocatorCi.device = m_device;

	if (vmaCreateAllocator(&vmaAllocatorCi, &m_allocator) != VK_SUCCESS){
		CORE_ERROR("[Vulkan Error] VMA allocator failed to be created!");
	}

};

	QueueFamilyIndices Device::FindQueueFamilies(VkPhysicalDevice& device) {
		QueueFamilyIndices indices;

		uint32_t queueFamilyCount = 0;
		vkGetPhysicalDeviceQueueFamilyProperties(device, &queueFamilyCount, nullptr);

		std::vector<VkQueueFamilyProperties> queueFamilies(queueFamilyCount);
		vkGetPhysicalDeviceQueueFamilyProperties(device, &queueFamilyCount, queueFamilies.data());
		int i = 0;
		for (const auto& queueFamily : queueFamilies) {
			if (queueFamily.queueFlags & VK_QUEUE_TRANSFER_BIT) {
				indices.graphicsFamily = i;
				indices.validGraphics = true;
			}
			VkBool32 presentSupport = false;
			vkGetPhysicalDeviceSurfaceSupportKHR(device, i, m_surface, &presentSupport);

			if (presentSupport) {
				indices.presentationFamily = i;
				indices.validPresentation = true;
			}

			if (indices.validGraphics && indices.validPresentation) {
				break;
			}
			indices.validGraphics = false;
			indices.validPresentation = false;
			i++;
		}

		return indices;
	}

	bool Device::IsCardCompatible(const VkPhysicalDevice& graphicsCard) {
		VkPhysicalDeviceProperties graphicsCardProperties;
		VkPhysicalDeviceFeatures graphicsCardFeatures;

		// Get the information about that graphics card's properties.
		vkGetPhysicalDeviceProperties(graphicsCard, &graphicsCardProperties);

		// Get the information about the graphics card's features.
		vkGetPhysicalDeviceFeatures(graphicsCard, &graphicsCardFeatures);

		CORE_INFO("[Vulkan Info] Compatibility check for graphics card: {}.", graphicsCardProperties.deviceName);

		std::string deviceType;
		switch(graphicsCardProperties.deviceType){
			case(VK_PHYSICAL_DEVICE_TYPE_DISCRETE_GPU): {
					deviceType = "Discrete GPU";
					break;
			}
			case(VK_PHYSICAL_DEVICE_TYPE_CPU): {
					deviceType = "CPU";
					break;
			}
			case(VK_PHYSICAL_DEVICE_TYPE_INTEGRATED_GPU): {
					deviceType = "Integrated GPU";
					break;
			}
			case(VK_PHYSICAL_DEVICE_TYPE_OTHER): {
					deviceType = "Other";
					break;
			}
			case(VK_PHYSICAL_DEVICE_TYPE_VIRTUAL_GPU): {
				deviceType = "Virtual GPU";
				break;
			}
			default:{ 
				deviceType = "Unknown";
				break;
			}
		}
		CORE_INFO("[Vulkan Info] Device type: {}", deviceType);

		bool swapchainSupported = false;

		std::uint32_t availableDeviceExtensions = 0;

		if (vkEnumerateDeviceExtensionProperties(graphicsCard, nullptr,
			&availableDeviceExtensions, nullptr) != VK_SUCCESS) {
			CORE_ERROR("[Vulkan Error] Extension properties failed for {}!", graphicsCardProperties.deviceName);
		}

		if (availableDeviceExtensions == 0) {
			CORE_ERROR("No Vulkan device extensions available!");
		}
		else {
			std::vector<VkExtensionProperties> deviceExtensions(availableDeviceExtensions);

			if (vkEnumerateDeviceExtensionProperties(graphicsCard, nullptr,
				&availableDeviceExtensions, deviceExtensions.data()) != VK_SUCCESS) {
				CORE_ERROR("[Vulkan Error] Extension properties failed for {}!", graphicsCardProperties.deviceName);
			}

			// Loop through all available device extensions and search for the requested one.
			for (const auto& i : deviceExtensions) {
				if (strcmp(i.extensionName, VK_KHR_SWAPCHAIN_EXTENSION_NAME) == 0) {
					swapchainSupported = true;
				}
			}
		}

		if (!swapchainSupported) {
			CORE_WARN("[Vulkan Warning] The device {} is not suitable because it doesn't support swapchain!", graphicsCardProperties.deviceName);
			return false;
		}

		VkBool32 presentationAvailable = 0;

		// Query if presentation is supported.
		if (vkGetPhysicalDeviceSurfaceSupportKHR(graphicsCard, 0, m_surface, &presentationAvailable)!= VK_SUCCESS) {
			CORE_ERROR("[Vulkan Error] Surface(presentation) check failed for device {}!", graphicsCardProperties.deviceName);
		}

		if (presentationAvailable == 0) {
			CORE_WARN("[Vulkan Warning] The device {} is not suitable because the presentation is not supported", graphicsCardProperties.deviceName);
			return false;
		}

		return true;
	};

	VkPhysicalDevice Device::ChooseGraphicsCard(const VkInstance& instance) {
		uint32_t availableCards = 0;

		if (vkEnumeratePhysicalDevices(instance, &availableCards, nullptr) != VK_SUCCESS) {
			CORE_ERROR("[Vulkan Error] Failed to enumerate physical devices!");
		}
		if (availableCards == 0){
			CORE_ERROR("[Vulkan Error] No vulkan compatible devices found!");
		}
		std::vector<VkPhysicalDevice> graphicsCards(availableCards);

		if (vkEnumeratePhysicalDevices(instance, &availableCards, graphicsCards.data()) != VK_SUCCESS) {
			CORE_ERROR("[Vulkan Error] Failed to enumerate physical devices!");
		}

		auto graphicsCardRatingLambda = [](const VkPhysicalDevice& graphicsCard) {
			size_t rating = 0;
			VkPhysicalDeviceMemoryProperties graphicsCardMemoryProperties;

			vkGetPhysicalDeviceMemoryProperties(graphicsCard, &graphicsCardMemoryProperties);

			//Go through all the memory heaps.
			for (size_t i = 0; i < graphicsCardMemoryProperties.memoryHeapCount; i++) {
				const auto &propertyFlag = graphicsCardMemoryProperties.memoryHeaps[i].flags;

				if (propertyFlag & VK_MEMORY_HEAP_DEVICE_LOCAL_BIT) {
					//GPU memory is the score factor, divide by a million bytes for readable rating results
					rating += graphicsCardMemoryProperties.memoryHeaps[i].size / (1000 * 1000);
				}
			}
			CORE_INFO("[Vulkan Info] Rating is {}", rating);
			return rating;
		};

		//Vector of valid cards in the graphicscards vector and their according rating
		std::vector<Pair<uint32_t, size_t>> cardsIndexRating;
		//Go through all the graphics cards available and pick the best one.
		for (uint32_t i = 0; i < availableCards; i++) {
			if (IsCardCompatible(graphicsCards[i])) {
				size_t rating = graphicsCardRatingLambda(graphicsCards[i]);
				cardsIndexRating.push_back(std::make_pair(i, rating));
			}
		}
		if (cardsIndexRating.size() == 0) {
			CORE_ERROR("[Vulkan Error] No graphics cards were suitable!");
		}

		size_t highestRating = 0;
		uint32_t chosenCard = 0;
		for (auto& i : cardsIndexRating) {
			if(i.second > highestRating){
				highestRating = i.second;
				chosenCard = i.first;
			}
		}
		return graphicsCards[chosenCard];
	};
		
	bool Device::IsExtensionSupported(const VkPhysicalDevice& gcard, const std::string& extension)const{
		uint32_t deviceExtensionCount = 0;

		if (vkEnumerateDeviceExtensionProperties(gcard, nullptr, &deviceExtensionCount, nullptr)!= VK_SUCCESS) {
			CORE_ERROR("[Vulkan Error] Extension enumeration failed!");
		}

		if (deviceExtensionCount == 0) {
			CORE_ERROR("Error: No Vulkan device extensions available!");
		}

		std::vector<VkExtensionProperties> deviceExtensions(deviceExtensionCount);

		if (vkEnumerateDeviceExtensionProperties(gcard, nullptr, &deviceExtensionCount,
			deviceExtensions.data()) != VK_SUCCESS) {
			CORE_ERROR("[Vulkan Error] Extension enumeration failed!");
		}
		for (auto& i : deviceExtensions) {
			if (i.extensionName == extension)
				return true;
		}
		return false;
	}
	bool Device::IsLayerSupported(const VkPhysicalDevice & gcard, const std::string & layer)const{
		uint32_t deviceLayerCount = 0;

		if (vkEnumerateDeviceLayerProperties(gcard, &deviceLayerCount, nullptr) != VK_SUCCESS) {
			CORE_ERROR("[Vulkan Error] Layer enumeration failed!");
		}

		if (deviceLayerCount == 0) {
			CORE_ERROR("Error: No Vulkan device layers available!");
		}

		std::vector<VkLayerProperties> deviceLayers(deviceLayerCount);

		if (vkEnumerateDeviceLayerProperties(gcard,&deviceLayerCount,
			deviceLayers.data()) != VK_SUCCESS) {
			CORE_ERROR("[Vulkan Error] Layer enumeration failed!");
		}
		for (auto& i : deviceLayers) {
			if (i.layerName == layer)
				return true;
		}
		return false;
	}
	bool Device::IsSurfaceSupported(const VkPhysicalDevice & gcard, VkSurfaceKHR surface)const{
		VkBool32 supported = false;

		// Query if presentation is supported.
		if (vkGetPhysicalDeviceSurfaceSupportKHR(gcard, 0, surface, &supported) != VK_SUCCESS) {
			CORE_ERROR("[Vukan Error] Surface support check failed!");
		}

		return supported;
	}
	Device::~Device(){
		if (m_allocator != nullptr) {
			vmaDestroyAllocator(m_allocator);
		}

		if (m_device != nullptr) {
			CORE_TRACE("[Vulkan Trace] Destroying logical device");
			vkDestroyDevice(m_device, nullptr);
		}
	}

	void Device::SetDebugMarkerName(void * object, VkDebugReportObjectTypeEXT objectType, const std::string & name) const {
#ifdef DEBUG
		if (m_renderDocEnabled) {
			VkDebugMarkerObjectNameInfoEXT info = {};
			info.sType = VK_STRUCTURE_TYPE_DEBUG_MARKER_OBJECT_NAME_INFO_EXT;
			info.objectType = objectType;
			info.object = reinterpret_cast<std::uint64_t>(object);
			info.pObjectName = name.c_str();

			if (m_vkDebugMarkerSetObjectName(m_device, &info) != VK_SUCCESS) {
				CORE_ERROR("[Vulkan Error] Failed to assign Vulkan debug marker name {}!  ", name);
			}
		}
#endif
	}
	void Device::AssignMemoryBlock(void * object, VkDebugReportObjectTypeEXT objectType, uint64_t name, size_t memorySize, const void * memoryBlock) const {
#ifdef DEBUG
		if (m_renderDocEnabled) {
			VkDebugMarkerObjectTagInfoEXT info = {};
			info.object = reinterpret_cast<std::uint64_t>(object);
			info.objectType = objectType;
			info.tagName = name;
			info.tagSize = memorySize;
			info.pTag = memoryBlock;

			if (m_vkDebugMarkerSetObjectTag(m_device, &info) != VK_SUCCESS) {
				CORE_ERROR("[Vulkan Error] Failed to assign Vulkan debug marker memory block!");
			}
		}
#endif
	}
	void Device::BindDebugRegion(VkCommandBuffer commandBuffer, const std::string & name, unsigned int color) const {
#ifdef DEBUG
		if (m_renderDocEnabled) {
			VkDebugMarkerMarkerInfoEXT marker = {};
			std::array<float, 4> arr = SwapUnsignedIntToFloatArray(color);

			std::copy(arr.begin(), arr.end(), marker.color);

			marker.pMarkerName = name.c_str();

			m_vkCmdDebugMarkerBegin(commandBuffer, &marker);
		}
#endif
	}
	void Device::InsertDebugMarker(VkCommandBuffer commandBuffer, const std::string & name, unsigned int color) const {
#ifdef DEBUG
		if (m_renderDocEnabled) {
			VkDebugMarkerMarkerInfoEXT marker = {};
			std::array<float, 4> arr = SwapUnsignedIntToFloatArray(color);

			std::copy(arr.begin(), arr.end(), marker.color);

			marker.pMarkerName = name.c_str();

			m_vkCmdDebugMarkerInsert(commandBuffer, &marker);
		}
#endif
	}
	void Device::EndDebugRegion(VkCommandBuffer commandBuffer) const {
#ifdef DEBUG
		if (m_renderDocEnabled) {
			m_vkCmdDebugMarkerEnd(commandBuffer);
		}
#endif
	}
}