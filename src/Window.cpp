#include <window/Window.h>
#include <utility/logging/Log.h>

WindowData::WindowData(std::string&& title, bool resizable, unsigned int width, unsigned int height, 
	WindowMode mode) : winTitle{ std::move(title) }, resizable{resizable}, width{ width }, height{ height }, winMode{ mode }{
};

WindowData::WindowData(WindowData&& data) : winTitle{ std::move(data.winTitle) }, resizable{std::move(data.resizable)},
width{ std::move(data.width) }, height{ std::move(data.height) }, winMode{ std::move(data.winMode) } {
}

Window* Window::CreateNewWindow(WindowData&& data, bool vsync) {
	return new Window(std::move(data), vsync);
}

void Window::Shutdown() {
	glfwTerminate();
}

void Window::SetUserPtr(void* ptr) {
	glfwSetWindowUserPointer(m_window, ptr);
}

void Window::SetFrameBufferCallback(GLFWframebuffersizefun frameCallback) {
	glfwSetFramebufferSizeCallback(m_window, frameCallback);
}

void Window::SetKeyButtonCallback(GLFWkeyfun keyCallback) {
	glfwSetKeyCallback(m_window, keyCallback);
};
void Window::SetMouseButtonCallback(GLFWmousebuttonfun mouseButtonCallback) {
	glfwSetMouseButtonCallback(m_window, mouseButtonCallback);
};
void Window::SetScrollCallback(GLFWscrollfun scrollCallback) {
	glfwSetScrollCallback(m_window, scrollCallback);
};
void Window::SetCursorPosCallback(GLFWcursorposfun cursorPosCallback) {
	glfwSetCursorPosCallback(m_window, cursorPosCallback);
};

Window::~Window() {
	glfwDestroyWindow(m_window);
}

Window::Window(WindowData&& data, bool vsync) : m_winData{ std::move(data) } {
	Init(vsync);
}

void Window::Draw() {
}

void Window::Init() {
	glfwInit();
}

void Window::Init(bool vsync) {
	glfwWindowHint(GLFW_CLIENT_API, GLFW_NO_API);
	glfwWindowHint(GLFW_RESIZABLE, m_winData.resizable);
	m_window = glfwCreateWindow((int)m_winData.width, (int)m_winData.height, m_winData.winTitle.c_str(), nullptr, nullptr);
	if (m_winData.winMode == WindowMode::Fullscreen) {
		GLFWmonitor* monitor = glfwGetPrimaryMonitor();
		const GLFWvidmode* mode = glfwGetVideoMode(monitor);
		glfwSetWindowMonitor(m_window, monitor, 0, 0, mode->width, mode->height, mode->refreshRate);
	}
#ifdef  DEBUG
	glfwSetErrorCallback([](int code, const char* description) {
		CORE_ERROR("[GLFW Error] Code:{}, Description: {}", code, description);
	});
#endif 	
	glfwSetWindowCloseCallback(m_window, [](GLFWwindow* window) {
	});
}

void Window::Resize(unsigned int width, unsigned int height) {
	m_winData.width = width;
	m_winData.height = height;
}
void Window::SetVSync(bool vsync){
}

bool Window::IsRunning() {
	return !glfwWindowShouldClose(m_window) ? true : false;
}

void Window::Update() {
	//Maybe something else too later?
	glfwWaitEvents();
}