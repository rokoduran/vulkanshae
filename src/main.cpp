#include <app/Application.h>
#include <utility/Clock.h>
#include <iostream>

using namespace vulkansetup;

int main(int arg, char** argv)
{
	Application::Initialize();
	Application* app = Application::CreateApplication();
	Clock c;
	std::cout << argv[0];
	double previous = c.GetSeconds();

	unsigned int counter = 0;

	while (app->Running()) {
		if (c.GetSeconds() - previous >= 1.0) {
			std::cout << counter << "fps" << std::endl;
			previous = c.GetSeconds();
			counter = 0;
		}
		counter++;
	}

	delete app;
	Application::Shutdown();
	return 0;
}