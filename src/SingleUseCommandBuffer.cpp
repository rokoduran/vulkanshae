#include <vulkansetup/SingleUseCommandBuffer.h>
#include <vulkansetup/Device.h>
#include <utility/logging/Log.h>

namespace vulkansetup{
   SingleUseCommandBuffer::SingleUseCommandBuffer(const Device& device)
   :m_device{device}, m_queue{device.GetGraphicsQueue()}, m_commandPool{device,VK_COMMAND_POOL_CREATE_TRANSIENT_BIT},
    m_recordingBegan{false}{
       CORE_INFO("[Vulkan Info] Created SingleUseCommandBuffer");
   }
   SingleUseCommandBuffer::~SingleUseCommandBuffer(){
       CORE_TRACE("[Vulkan Trace] Destroying SingleUseCommandBuffer");
   };
   void SingleUseCommandBuffer::CreateCommandBuffer(){
       if(m_singleUseCommandBuffer!=VK_NULL_HANDLE){
           CORE_WARN("[Vulkan Warning] Attempt to single use command buffer made while it already exists!");
           return;
       }
       	VkCommandBufferAllocateInfo allocInfo{};
        allocInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
        allocInfo.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
        allocInfo.commandPool = m_commandPool.GetCommandPool();
        allocInfo.commandBufferCount = 1;
        if(vkAllocateCommandBuffers(m_device.GetLogicDevice(), &allocInfo, &m_singleUseCommandBuffer)!= VK_SUCCESS){
            CORE_ERROR("[Vulkan Error] Failed to create single use command buffer from Command pool!");
        }
   }
   void SingleUseCommandBuffer::BeginRecording(){
       if(m_recordingBegan && m_singleUseCommandBuffer!= VK_NULL_HANDLE){
           CORE_WARN("[Vulkan Warning] Single use command buffer recording attempt occured even though it's already recording!");
           return;
       }
        m_recordingBegan = true;
       	VkCommandBufferBeginInfo beginInfo{};
	    beginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
	    beginInfo.flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT;

	    if(vkBeginCommandBuffer(m_singleUseCommandBuffer, &beginInfo) != VK_SUCCESS){
            CORE_ERROR("[Vulkan Error] Failed to start recording single use command buffer!");
        }
   }
   void SingleUseCommandBuffer::EndRecordingAndSubmit(){
        if(!m_recordingBegan && m_singleUseCommandBuffer!= VK_NULL_HANDLE){
            CORE_WARN("[Vulkan Warning] Single use command buffer end recording attempt occured even though it's not even recording!");
           return;
        }
        if(vkEndCommandBuffer(m_singleUseCommandBuffer) != VK_SUCCESS){
            CORE_ERROR("[Vulkan Error] Failed to end recording single use command buffer!");
        }

        VkSubmitInfo submitInfo{};
        submitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
        submitInfo.commandBufferCount = 1;
        submitInfo.pCommandBuffers = &m_singleUseCommandBuffer;

        vkQueueSubmit(m_queue, 1, &submitInfo, VK_NULL_HANDLE);
        vkQueueWaitIdle(m_queue);

        vkFreeCommandBuffers(m_device.GetLogicDevice(), m_commandPool.GetCommandPool(), 1, &m_singleUseCommandBuffer);
   }
}