#include <vulkansetup/DebugReport.h>
#include <utility/logging/Log.h>

namespace vulkansetup {

	DebugReport::DebugReport(VkInstance instance) {
		CORE_INFO("[Vulkan Info] Creating debug messenger...");
		VkDebugUtilsMessengerCreateInfoEXT createInfo;
		PopulateDebugMessengerCreateInfo(createInfo);

		if (CreateDebugUtilsMessengerEXT(instance, &createInfo, nullptr, &m_debugMessenger) != VK_SUCCESS) {
			CORE_ERROR("[Vulkan Error] Failed to create debug messenger!");
		}
	}

	VkResult DebugReport::CreateDebugUtilsMessengerEXT(VkInstance& instance, const VkDebugUtilsMessengerCreateInfoEXT* pCreateInfo, const VkAllocationCallbacks* pAllocator, VkDebugUtilsMessengerEXT* pDebugMessenger) {
		auto func = (PFN_vkCreateDebugUtilsMessengerEXT)vkGetInstanceProcAddr(instance, "vkCreateDebugUtilsMessengerEXT");
		if (func != nullptr) {
			return func(instance, pCreateInfo, pAllocator, pDebugMessenger);
		}
		else {
			return VK_ERROR_EXTENSION_NOT_PRESENT;
		}
	};

	void DebugReport::DestroyDebugUtilsMessengerEXT(const VkAllocationCallbacks* pAllocator, VkInstance instance) {
		auto func = (PFN_vkDestroyDebugUtilsMessengerEXT)vkGetInstanceProcAddr(instance, "vkDestroyDebugUtilsMessengerEXT");
		if (func != nullptr) {
			func(instance, m_debugMessenger, pAllocator);
		}
		CORE_TRACE("[Vulkan Trace] Destroyed Debug Messenger");
	}

	void DebugReport::PopulateDebugMessengerCreateInfo(VkDebugUtilsMessengerCreateInfoEXT& createInfo) {
		createInfo = {};
		createInfo.sType = VK_STRUCTURE_TYPE_DEBUG_UTILS_MESSENGER_CREATE_INFO_EXT;
		createInfo.messageSeverity = VK_DEBUG_UTILS_MESSAGE_SEVERITY_VERBOSE_BIT_EXT | VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT | VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT;
		createInfo.messageType = VK_DEBUG_UTILS_MESSAGE_TYPE_GENERAL_BIT_EXT | VK_DEBUG_UTILS_MESSAGE_TYPE_VALIDATION_BIT_EXT | VK_DEBUG_UTILS_MESSAGE_TYPE_PERFORMANCE_BIT_EXT;
		createInfo.pfnUserCallback = DebugCallback;
	}

	VKAPI_ATTR VkBool32 VKAPI_CALL DebugReport::DebugCallback(VkDebugUtilsMessageSeverityFlagBitsEXT messageSeverity, VkDebugUtilsMessageTypeFlagsEXT messageType, const VkDebugUtilsMessengerCallbackDataEXT* pCallbackData, void* pUserData) {
		if (messageSeverity > VK_DEBUG_UTILS_MESSAGE_SEVERITY_VERBOSE_BIT_EXT) {
			std::string type, severity;
			switch (messageType) {
			case VK_DEBUG_UTILS_MESSAGE_TYPE_GENERAL_BIT_EXT: {
				type = "General";
				break;
			}
			case VK_DEBUG_UTILS_MESSAGE_TYPE_VALIDATION_BIT_EXT: {
				type = "Validation";
				break;
			}
			case VK_DEBUG_UTILS_MESSAGE_TYPE_PERFORMANCE_BIT_EXT: {
				type = "Performance";
				break;
			}
			default:break;
			}

			switch (messageSeverity) {
			case VK_DEBUG_UTILS_MESSAGE_SEVERITY_INFO_BIT_EXT: {
				severity = "Info";
				CORE_INFO("[Vulkan {}] Type:{}\n Message:{}\n", severity, type, pCallbackData->pMessage);
				break;
			}
			case VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT: {
				severity = "Warning";
				CORE_WARN("[Vulkan {}] Type:{}\n Message:{}\n", severity, type, pCallbackData->pMessage);
				break;
			}
			case VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT: {
				severity = "Error";
				CORE_ERROR("[Vulkan {}] Type:{}\n Message:{}\n", severity, type, pCallbackData->pMessage);
				break;
			}
			default:break;
			}
		}
		return VK_FALSE;
	}
}