#include <vulkansetup/Surface.h>
#include <utility/logging/Log.h>

namespace vulkansetup{

	Surface::Surface(VkInstance instance, GLFWwindow* window) : m_instance{instance} {
		CORE_INFO("[Vulkan Info] Creating window surface");
		if (glfwCreateWindowSurface(m_instance, window, nullptr, &m_surface) != VK_SUCCESS) {
			CORE_ERROR("[Vulkan Error] Failed to create window surface!");
		}
	}

	Surface::~Surface() {
		CORE_TRACE("[Vulkan Trace] Destroying window surface");
		vkDestroySurfaceKHR(m_instance, m_surface, nullptr);
	}

}