#include <vulkansetup/VertexIndexBuffer.h>
#include <vulkansetup/Device.h>

namespace vulkansetup{
    VertexIndexBuffer::VertexIndexBuffer(const Device& device, const std::string& debugName, const VkDeviceSize& size)
    :BaseGPUBuffer(device, debugName, size, VK_BUFFER_USAGE_TRANSFER_DST_BIT | VK_BUFFER_USAGE_VERTEX_BUFFER_BIT
    | VK_BUFFER_USAGE_INDEX_BUFFER_BIT, VMA_MEMORY_USAGE_CPU_ONLY){

    }
    void VertexIndexBuffer::SubmitVertices(void* data, size_t dataSize){

    }
    void SubmitIndices(void* data, size_t dataSize){

    }
}