#include <vulkansetup/Framebuffer.h>
#include <vulkansetup/Device.h>
#include <utility/logging/Log.h>

namespace vulkansetup{
    Framebuffer::Framebuffer(const Device& device, VkRenderPass renderPass, VkExtent2D extent, const std::vector<VkImageView>& viewAttachments, const std::string& debugName)
    :m_device{device}, m_debugName{debugName}{
        CORE_INFO("[Vulkan Info] Creating framebuffer {}", m_debugName);

		VkFramebufferCreateInfo framebufferInfo{};
		framebufferInfo.sType = VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO;
		framebufferInfo.renderPass = renderPass;
		framebufferInfo.attachmentCount = (uint32_t) viewAttachments.size();
		framebufferInfo.pAttachments = viewAttachments.data();
		framebufferInfo.width = extent.width;
		framebufferInfo.height = extent.height;
		framebufferInfo.layers = 1;

		if (vkCreateFramebuffer(m_device.GetLogicDevice(), &framebufferInfo, nullptr, &m_framebuffer) != VK_SUCCESS) {
			CORE_ERROR("[Vulkan Error] Failed to create framebuffer {}!", m_debugName);
		}
        m_device.SetDebugMarkerName(m_framebuffer, VK_DEBUG_REPORT_OBJECT_TYPE_FRAMEBUFFER_EXT, m_debugName);
    }

    Framebuffer::~Framebuffer(){
        if(m_framebuffer!= VK_NULL_HANDLE){
            vkDestroyFramebuffer(m_device.GetLogicDevice(), m_framebuffer, nullptr);
        }
    }
}