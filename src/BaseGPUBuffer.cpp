#include <vulkansetup/BaseGPUBuffer.h>
#include <vulkansetup/Device.h>
#include <utility/logging/Log.h>

namespace vulkansetup{
    BaseGPUBuffer::BaseGPUBuffer(const Device& device, const std::string& debugName, const VkDeviceSize& size,
    const VkBufferUsageFlags& bufferUsage, const VmaMemoryUsage& memoryUsage)
    : m_device{device}, m_debugName{debugName}, m_bufferSize{size}{

            CORE_INFO("[Vulkan Info] Creating buffer {} of size {}", m_debugName, size);
            VkBufferCreateInfo bufferInfo{};
            bufferInfo.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
            bufferInfo.size = size;
            bufferInfo.usage = bufferUsage;
            bufferInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;

            //Mapping and unmapping shouldn't be an issue an any platform except windows 7/8
            m_allocCreateInfo.flags = VMA_ALLOCATION_CREATE_MAPPED_BIT;
            m_allocCreateInfo.usage = memoryUsage;

            if(vmaCreateBuffer(m_device.GetAllocator(), &bufferInfo, &m_allocCreateInfo, &m_buffer, &m_allocation, &m_allocationInfo) != VK_SUCCESS){
                CORE_ERROR("[Vulkan Error] Failed to create GPU buffer {}!", m_debugName);
            }
            m_device.SetDebugMarkerName(m_buffer, VK_DEBUG_REPORT_OBJECT_TYPE_BUFFER_EXT, m_debugName);

    }
    BaseGPUBuffer::BaseGPUBuffer(const Device& device, const std::string& debugName, const VkDeviceSize& size, void* dataToFill, size_t dataSize,
    const VkBufferUsageFlags& bufferUsage, const VmaMemoryUsage& memoryUsage)
    : BaseGPUBuffer(device, debugName, size, bufferUsage, memoryUsage){
        memcpy(m_allocationInfo.pMappedData, dataToFill, dataSize);
    }

    BaseGPUBuffer::BaseGPUBuffer(BaseGPUBuffer&& other) noexcept
    :m_debugName(std::move(other.GetName())), m_device(other.m_device), m_buffer(std::exchange(other.m_buffer, nullptr)),
      m_allocation(std::exchange(other.m_allocation, nullptr)), m_allocationInfo(std::move(other.GetAllocationInfo())),
      m_allocCreateInfo(std::move(other.GetCreateInfo())) {

    }

    BaseGPUBuffer::~BaseGPUBuffer(){
        if(m_buffer != VK_NULL_HANDLE){
            vmaDestroyBuffer(m_device.GetAllocator(), m_buffer, m_allocation);
        }
    };
}