#include <window/Input.h>


bool Input::IsKeyPressed(KeyButtons key) {
	return m_pressedKeyboardKeys[key];
};

bool Input::IsKeyHeld(KeyButtons key) {
	return m_repeatedKeyboardKeys[key];
};
bool Input::IsMouseButtonPressed(KeyButtons button) {
	return m_mouseKeys[button];
};

glm::vec2& Input::GetMousePos() {
	return m_currentMousePos;
};

void Input::SetScrollValue(float value) {
	m_scrollValue = value;
}

glm::vec2 Input::CalcMouseDelta(){
	return m_currentMousePos - m_previousMousePos;
}
