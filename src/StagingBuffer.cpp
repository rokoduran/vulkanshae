#include <vulkansetup/StagingBuffer.h>
#include <vulkansetup/Device.h>
namespace vulkansetup{

    StagingBuffer::StagingBuffer(const Device& device, const std::string& debugName, VkDeviceSize bufferSize, void* dataToFill, size_t dataSize)
    : BaseGPUBuffer(device, debugName, bufferSize, dataToFill, dataSize, VK_BUFFER_USAGE_TRANSFER_SRC_BIT, VMA_MEMORY_USAGE_CPU_ONLY),
     m_singleUseCmdBuffer{device}{ 
     };

    void StagingBuffer::TransferDataToBuffer(BaseGPUBuffer& other, VkDeviceSize sourceOffset, VkDeviceSize destinationOffset){
        m_singleUseCmdBuffer.CreateCommandBuffer();
        m_singleUseCmdBuffer.BeginRecording();

        VkBufferCopy copyRegion{};
        copyRegion.srcOffset = sourceOffset;
        copyRegion.dstOffset = destinationOffset; // Optional
        copyRegion.size = m_bufferSize;

        vkCmdCopyBuffer(m_singleUseCmdBuffer.GetCommandBuffer(), m_buffer, other.GetBuffer(), 1, &copyRegion);

        m_singleUseCmdBuffer.EndRecordingAndSubmit();
        
    }
}