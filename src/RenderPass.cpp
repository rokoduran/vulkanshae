#include <vulkansetup/RenderPass.h>
#include <vulkansetup/Device.h>
#include <utility/logging/Log.h>

namespace vulkansetup{
    RenderPass::RenderPass(const Device& device, const std::string& debugName, const std::vector<VkAttachmentDescription>& attachmentDescripts,
    const std::vector<VkSubpassDependency>& dependencies, VkSubpassDescription subpassDescription)
    : m_device{device}, m_debugName{debugName}{

        VkRenderPassCreateInfo renderPassInfo = {};
        renderPassInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO;
        renderPassInfo.attachmentCount = (uint32_t)(attachmentDescripts.size());
        renderPassInfo.pAttachments = attachmentDescripts.data();
        renderPassInfo.subpassCount = 1;
        renderPassInfo.pSubpasses = &subpassDescription;
        renderPassInfo.dependencyCount = (uint32_t)(dependencies.size());
        renderPassInfo.pDependencies = dependencies.data();

        if (vkCreateRenderPass(m_device.GetLogicDevice(), &renderPassInfo, nullptr, &m_renderPass) != VK_SUCCESS) {
            CORE_ERROR("[Vulkan Error] Failed to initialise render pass!");
        }
        CORE_INFO("[Vulkan Info] Created render pass {}", m_debugName);
    }
    RenderPass::~RenderPass(){
        CORE_TRACE("[Vulkan Trace] Destroying render pass {}", m_debugName);
        vkDestroyRenderPass(m_device.GetLogicDevice(), m_renderPass, nullptr);
    }
}