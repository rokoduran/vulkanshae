#include <vulkansetup/CommandPool.h>
#include <vulkansetup/Device.h>
#include <utility/logging/Log.h>

namespace vulkansetup{

    CommandPool::CommandPool(const Device& device, VkCommandPoolCreateFlagBits flags)
    :m_device{device}{
        CORE_INFO("[Vulkan Info] Creating Command pool...");

        VkCommandPoolCreateInfo poolInfo{};
        poolInfo.sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO;
        poolInfo.queueFamilyIndex = device.GetGraphicsFamilyIndex();
        poolInfo.flags = flags;

        if(vkCreateCommandPool(m_device.GetLogicDevice(),&poolInfo, nullptr, &m_commandPool) != VK_SUCCESS){
            CORE_ERROR("[Vulkan Error] Command pool failed to be created!");
        }
    }
    CommandPool::~CommandPool(){
        if(m_commandPool != VK_NULL_HANDLE){
            vkDestroyCommandPool(m_device.GetLogicDevice(), m_commandPool, nullptr);
            CORE_TRACE("[Vulkan Trace] Destroyed command pool");
        }
    }
}