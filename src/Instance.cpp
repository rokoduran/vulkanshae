#include <vulkansetup/Instance.h>
#include <GLFW/glfw3.h>
#include <utility/logging/Log.h>

namespace vulkansetup {
	bool Instance::IsExtensionSupported(const std::string& extension) {
		uint32_t instanceExtensionCount = 0;
		//First call to the function to know how many instance extensions are there available.
		if (vkEnumerateInstanceExtensionProperties(nullptr, &instanceExtensionCount, nullptr) != VK_SUCCESS) {
			CORE_ERROR("[Vulkan Error] Enumerate instance extensions failed!");
		}

		if (instanceExtensionCount == 0) {
			CORE_ERROR("[Vulkan Error] No instance extensions available!");
		}

		std::vector<VkExtensionProperties> instanceExtensions(instanceExtensionCount);

		//Second to store all the extensions now that the size is known.
		if (vkEnumerateInstanceExtensionProperties(nullptr, &instanceExtensionCount, instanceExtensions.data())!= VK_SUCCESS) {
			CORE_ERROR("[Vulkan Error] Storing instance extensions failed!");
		}

		// Search the container for the requested instance extension.
		for (auto& i : instanceExtensions) {
			if (i.extensionName == extension)
				return true;
		}
		return false;
	}

	bool Instance::IsValidationLayerSupported(const std::string& layer) {
		uint32_t instanceLayerCount = 0;

		//First call to the function to know how many instance layers are there available.
		if (vkEnumerateInstanceLayerProperties(&instanceLayerCount, nullptr) != VK_SUCCESS) {
			CORE_ERROR("[Vulkan Error] Enumerate instance layer failed!");
		}

		if (instanceLayerCount == 0) {
			CORE_ERROR("[Vulkan Error] No instance layers available!");
		}

		std::vector<VkLayerProperties> instanceLayers(instanceLayerCount);

		//Second to store all the layers now that the size is known.
		if (vkEnumerateInstanceLayerProperties(&instanceLayerCount, instanceLayers.data()) != VK_SUCCESS) {
			CORE_ERROR("[Vulkan Error] Storing instance layers failed!");
		}

		// Search the container for the requested layer extension.
		for (auto& i : instanceLayers) {
			if (i.layerName == layer)
				return true;
		}
		return false;
	};

	Instance::Instance(const std::string& appName, uint32_t appVersion, uint32_t vulkanApiVersion, bool enableValidLayers,
		bool& enableRenderDoc, std::vector<std::string> requiredExtensions, std::vector<std::string> requestedLayers) {
		CORE_INFO("[Vulkan Info] Creating Vulkan instance...");
		VkApplicationInfo appInfo = {};
		appInfo.sType = VK_STRUCTURE_TYPE_APPLICATION_INFO;
		appInfo.pApplicationName = appName.empty() ? "Shae Engine App" : appName.c_str();
		appInfo.pEngineName = "Shae";
		appInfo.applicationVersion = appVersion;
		appInfo.engineVersion = VK_MAKE_VERSION(1,0,0);
		appInfo.apiVersion = vulkanApiVersion;
		//First, deal with the instance extensions

		//Wanted instance extensions.
		std::vector<const char*> instanceExtensions = {
#ifdef DEBUG
			VK_EXT_DEBUG_UTILS_EXTENSION_NAME,
			VK_EXT_DEBUG_REPORT_EXTENSION_NAME
#endif
		};

		uint32_t glfwExtensionCount = 0;

		// Get count of glfw extensions for platform
		auto* glfwExtensions = glfwGetRequiredInstanceExtensions(&glfwExtensionCount);

		if (glfwExtensionCount == 0) {
			CORE_ERROR("[Vulkan Error] No glfw extensions found!");
		}

		//Add instance extensions which are required by GLFW to the wanted extensions
		for (uint32_t i = 0; i < glfwExtensionCount; i++) {
			instanceExtensions.push_back(glfwExtensions[i]);
		}
		CORE_INFO("[Vulkan Info] All instance extensions: {}", fmt::join(instanceExtensions, ", "));
		
		//Add requested extensions to the wanted ones
		for (auto& i : requiredExtensions) {
			//Dupes are more than likey to happen but don't matter
			instanceExtensions.push_back(i.c_str());
		}

		std::vector<const char*> enabledInstanceExtensions;

		for (auto& i : instanceExtensions) {
			if (IsExtensionSupported(i)) {
				CORE_INFO("[Vulkan Info] {} added to enabled extensions.", i);
				enabledInstanceExtensions.push_back(i);
			}
			else {
				CORE_WARN("[Vulkan Warning] {} was a requested extension but is unavailable.", i);
			}
		}
		
		//Next, deal with the instance layers

		std::vector<const char*> instanceLayers;
		
		if (enableRenderDoc) {
			instanceLayers.push_back("VK_LAYER_RENDERDOC_Capture");
		}

		if (enableValidLayers) {
			CORE_INFO("[Vulkan Info] Validation layers enabled.");
			instanceLayers.push_back("VK_LAYER_KHRONOS_validation");
			//This layer is a test for AMD Radeon cards, as some of them don't have KHRONOS_validation
			instanceLayers.push_back("VK_LAYER_LUNARG_standard_validation");
		}

		for (const auto& i : requestedLayers) {
			instanceLayers.push_back(i.c_str());
		}

		std::vector<const char*> enabledInstanceLayers;

		for (const auto& i : instanceLayers) {
			if (IsValidationLayerSupported(i)) {
				CORE_INFO("[Vulkan Info] {} added to enabled layers.", i);
				enabledInstanceLayers.push_back(i);
			}
			else {
				CORE_WARN("[Vulkan Warning] {} was a requested layer but is unavailable.", i);
				if (enableRenderDoc) {
					if (i == "VK_LAYER_RENDERDOC_Capture") {
						enableRenderDoc = false;
					}
				}
			}
		}

		VkInstanceCreateInfo createInfo = {};
		createInfo.sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO;
		createInfo.pApplicationInfo = &appInfo;
		createInfo.ppEnabledExtensionNames = enabledInstanceExtensions.data();
		createInfo.enabledExtensionCount = static_cast<uint32_t>(enabledInstanceExtensions.size());
		createInfo.ppEnabledLayerNames = enabledInstanceLayers.data();
		createInfo.enabledLayerCount = static_cast<uint32_t>(enabledInstanceLayers.size());

		if (vkCreateInstance(&createInfo, nullptr, &m_instance) != VK_SUCCESS) {
			CORE_ERROR("[Vulkan Error] Creating VkInstance failed!");
		}

	};

	Instance::~Instance() {
		if (m_instance != nullptr) {
			CORE_TRACE("[Vulkan Trace] Destroying VK instance");
			vkDestroyInstance(m_instance, nullptr);
		}
	}

}