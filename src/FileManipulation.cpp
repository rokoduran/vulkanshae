#include <utility/FileManipulation.h>
#include <fstream>
#include <cstring>

String FileManipulation::ReadFileToStr(String& path) {
	std::ifstream file(path);
	if (file.is_open()) {
		file.seekg(0, file.end);
		int length = (int)file.tellg();
		file.seekg(0, file.beg);
		char* data = new char[length + 1];
		memset(data, 0, length);
		file.read(data, length);
		String str(data);
		delete[] data;
		return str;
	}
	return NULL;
}

String FileManipulation::ReadFileToStr(String path) {
	std::ifstream file(path);
	if (file.is_open()) {
		file.seekg(0, file.end);
		int length = (int)file.tellg();
		file.seekg(0, file.beg);
		char* data = new char[length + 1];
		memset(data, 0, length);
		file.read(data, length);
		String str(data);
		delete[] data;
		return str;
	}
	return NULL;
}


std::vector<char> FileManipulation::ReadFileToVec(String& path) {
	std::ifstream file(path,std::ios::ate | std::ios::binary);
	if (file.is_open()) {
		file.seekg(0, file.end);
		int length = (int)file.tellg();
		file.seekg(0, file.beg);
		std::vector<char> data(length);
		file.read(data.data(), length);
		return data;
	}
	return std::vector<char>();
}