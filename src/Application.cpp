#include <app/Application.h>
#include <utility/logging/Log.h>
#include <iostream>

using namespace vulkansetup;

void Application::Initialize() {
	Log::Init();
	Window::Init();
}

void Application::Shutdown() {
	Window::Shutdown();
};

Application::Application(std::string&& title, bool resizable,
	unsigned int width, unsigned int height, WindowMode mode, bool vsync) : m_window{ 
		Window::CreateNewWindow(WindowData(std::move(title),resizable,width,height,mode), vsync) } {

	Init();
};

Application::Application(WindowData&& data, bool vsync) : m_window{ Window::CreateNewWindow(std::move(data), vsync) } {
	Init();
};

void Application::KeyButtonCallback(int key, int action) {
	input.m_pressedKeyboardKeys[key] = action != GLFW_RELEASE;
	input.m_repeatedKeyboardKeys[key] = action == GLFW_REPEAT;
};
void Application::MouseButtonCallback(int key, int action) {
	input.m_mouseKeys[key] = action != GLFW_RELEASE;
};
void Application::MousePosCallback(double x, double y) {
	input.m_previousMousePos = input.m_currentMousePos;
	glm::vec2& currentPos = input.m_currentMousePos;
	currentPos.x = (float)x;
	currentPos.y = (float)y;
};
void Application::ScrollCallback() {
};
void Application::FrameBufferResizeCallback(int width, int height) {
	m_window->Resize(width, height);
	GLFWwindow* window = m_window->GetWindow();
	while(width==0 || height==0){
		glfwGetFramebufferSize(window, &width, &height);
		glfwWaitEvents();
	}
	m_swapchain->RecreateSwapchain(width, height);
};

void Application::Init() {
	std::string app = "Shae test";
	std::vector<std::string> requiredExtensions;
	std::vector<std::string> requestedLayers;
	//Load config from some file later.
	bool renderDocEnabled;
#ifdef DEBUG
	renderDocEnabled = false;
	m_instance = std::make_unique<Instance>(app, VK_MAKE_VERSION(1,0,0), VK_API_VERSION_1_0, true, renderDocEnabled, requiredExtensions, requestedLayers);
	m_surface = std::make_unique<Surface>(m_instance->GetInstance(), m_window->GetWindow());
	m_debugReport = std::make_unique<DebugReport>(m_instance->GetInstance());
	m_device = std::make_unique<Device>(m_instance->GetInstance(), m_surface->GetSurface(), renderDocEnabled);
	std::string swapName = "Swappy";
	m_swapchain = std::make_unique<Swapchain>(*m_device, m_surface->GetSurface(), m_window->GetWidth(), m_window->GetHeight(), swapName, false);
#else
	renderDocEnabled = false;
	m_instance = std::make_unique<Instance>(app, VK_MAKE_VERSION(1, 0, 0), VK_API_VERSION_1_0, false, renderDocEnabled, requiredExtensions, requestedLayers);
	m_surface = std::make_unique<Surface>(m_instance->GetInstance(), m_window->GetWindow());
	m_debugReport = std::make_unique<DebugReport>(m_instance->GetInstance());
	m_device = std::make_unique<Device>(m_instance->GetInstance(), m_surface->GetSurface(), renderDocEnabled);
#endif
	
	m_window->SetUserPtr(this);
	m_window->SetKeyButtonCallback([](GLFWwindow* window, int key, int scancode, int action, int mods) {
		auto userPtr = (Application*)glfwGetWindowUserPointer(window);
		userPtr->KeyButtonCallback(key, action);
	});
	m_window->SetMouseButtonCallback([](GLFWwindow* window, int key, int action, int mods) {
		auto userPtr = (Application*)glfwGetWindowUserPointer(window);
		userPtr->MouseButtonCallback(key, action);
	});
	m_window->SetFrameBufferCallback([](GLFWwindow *window, int width, int height) {
		auto userPtr = (Application*)glfwGetWindowUserPointer(window);
		userPtr->FrameBufferResizeCallback(width, height);
	});
	m_window->SetCursorPosCallback([](GLFWwindow* window, double x, double y) {
		auto userPtr = (Application*)glfwGetWindowUserPointer(window);
		userPtr->MousePosCallback(x, y);
	});
}

Application* Application::CreateApplication() {
	return new Application();
};

bool Application::Running() {
	static bool vsync = false;
	if (m_window->IsRunning()) {
		m_window->Update();
		m_window->Draw();
		if (input.IsKeyPressed(KeyButtons::KEYBOARD_LEFT)) {
			m_window->SetVSync(vsync);
			vsync = !vsync;
		}
		return true;
	};
	return false;
}