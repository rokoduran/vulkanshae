#include <vulkansetup/Swapchain.h>
#include <vulkansetup/Device.h>
//#include <vulkansetup/Semaphore.h">
#include <utility/logging/Log.h>

namespace vulkansetup{

    Swapchain::Swapchain(const Device& device, VkSurfaceKHR surface, uint32_t windowWidth, uint32_t windowHeight,
     const std::string& name, bool vsync)
    : m_device{device}, m_surface{surface}, m_debugName{name}, m_vsync(vsync){
        CORE_INFO("[Vulkan Info] Creating swapchain...");
        CreateSwapchain(VK_NULL_HANDLE, windowWidth, windowHeight);
    }
    Swapchain::~Swapchain(){
        if (m_swapchain != nullptr) {
            CORE_TRACE("[Vulkan Trace] Destroying swapchain {}", m_debugName);
            vkDestroySwapchainKHR(m_device.GetLogicDevice(), m_swapchain, nullptr);
        }

        for (auto* image_view : m_swapchainImageViews) {
            if (image_view != nullptr) {
                vkDestroyImageView(m_device.GetLogicDevice(), image_view, nullptr);
            }
        }
    };

    VkImageView Swapchain::GetImageViewAtIndex(uint32_t index)const{
        if(index >= m_swapchainImageCount){
            CORE_ERROR("[Vulkan Error] Requested image view index {} is bigger than the image view count ({})!", index, m_swapchainImageCount);
        }
        return m_swapchainImageViews[index];
    };

    void Swapchain::ToggleVsync(bool vsync){
        if(vsync != m_vsync){
            m_vsync = vsync;
            CORE_INFO("[Vulkan Info] Vsync toggled {}", m_vsync);

            uint32_t width, height;
            width = m_extent.width;
            height = m_extent.height;

            RecreateSwapchain(width, height);
        }
    };
/*uint32_t Swapchain::AcquireNextImage(const Semaphore &semaphore) {
    std::uint32_t image_index;
    vkAcquireNextImageKHR(m_device.device(), m_swapchain, std::numeric_limits<std::uint64_t>::max(), semaphore.get(),
                          VK_NULL_HANDLE, &image_index);
    return image_index;
*/
    void Swapchain::RecreateSwapchain(uint32_t windowWidth, uint32_t windowHeight){
        CORE_INFO("[Vulkan Info] Recreating swapchain with dimensions {} & {}", windowWidth, windowHeight);
        VkSwapchainKHR old_swapchain = m_swapchain;
        VkDevice logicalDevice = m_device.GetLogicDevice();

        for (auto& image_view : m_swapchainImageViews) {
            vkDestroyImageView(logicalDevice, image_view, nullptr);
        }

        m_swapchainImageViews.clear();

        m_swapchainImages.clear();

        CreateSwapchain(old_swapchain, windowWidth, windowHeight);
    }
    void Swapchain::CreateSwapchain(VkSwapchainKHR oldSwapchain, uint32_t windowWidth, uint32_t windowHeight){
        auto transform = GetSurfaceCapabilitiesData(windowWidth, windowHeight);
        auto presentationMode = ChoosePresentationMode();
        ChooseSurfaceColorFormat();
        VkSurfaceTransformFlagBitsKHR flagBits = {};
        VkSwapchainCreateInfoKHR createInfo = {};
        createInfo.sType = VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR;
        createInfo.surface = m_surface;
        createInfo.minImageCount = m_swapchainImageCount;
        createInfo.imageFormat = m_surfaceFormat.format;
        createInfo.imageColorSpace = m_surfaceFormat.colorSpace;
        createInfo.imageExtent.width = m_extent.width;
        createInfo.imageExtent.height = m_extent.height;
        createInfo.imageUsage = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT;
        createInfo.preTransform = transform;
        createInfo.imageArrayLayers = 1;
        uint32_t familyIndices[] = {m_device.GetGraphicsFamilyIndex(), m_device.GetPresentationFamilyIndex()};
        if(familyIndices[0] != familyIndices[1]){
            createInfo.imageSharingMode = VK_SHARING_MODE_CONCURRENT;
            createInfo.queueFamilyIndexCount = 2;
            createInfo.pQueueFamilyIndices = familyIndices;
        }
        else{
            createInfo.imageSharingMode = VK_SHARING_MODE_EXCLUSIVE;
            createInfo.queueFamilyIndexCount = 0;
            createInfo.pQueueFamilyIndices = nullptr;
        }
        createInfo.presentMode = presentationMode;
         // Setting clipped to VK_TRUE allows the implementation to discard rendering outside of the surface area.
        createInfo.clipped = VK_TRUE;
        //If this is causing issue on some surfaces, use a vector of alpha flags and check which are available in surface capabilities
        createInfo.compositeAlpha = VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR;
        //It's either this or VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT.
        createInfo.imageUsage |= VK_IMAGE_USAGE_TRANSFER_SRC_BIT;

        if(oldSwapchain!= VK_NULL_HANDLE){
            createInfo.oldSwapchain = oldSwapchain;
        }
        VkDevice logicalDevice = m_device.GetLogicDevice();

        if (vkCreateSwapchainKHR(logicalDevice, &createInfo, nullptr, &m_swapchain) != VK_SUCCESS) {
		    CORE_ERROR("[Vulkan Error] Failed to create swapchain!");
	    }

        if (vkGetSwapchainImagesKHR(logicalDevice, m_swapchain, &m_swapchainImageCount, nullptr) != VK_SUCCESS) {
            CORE_ERROR("[Vulkan Error] Failed to get swapchain images!");
	    }
        CORE_INFO("[Vulkan Info] Swapchain image count is {}", m_swapchainImageCount);

        m_swapchainImages.resize(m_swapchainImageCount);

        if (vkGetSwapchainImagesKHR(logicalDevice, m_swapchain, &m_swapchainImageCount, m_swapchainImages.data()) != VK_SUCCESS) {
            CORE_ERROR("[Vulkan Error] Failed to get swapchain images!");
	    }

        m_device.SetDebugMarkerName(m_swapchain, VK_DEBUG_REPORT_OBJECT_TYPE_SWAPCHAIN_KHR_EXT, m_debugName);


    }

    void Swapchain::CreateImageViews(){
        m_swapchainImageViews.resize(m_swapchainImageCount);

        VkImageViewCreateInfo createInfo = {};
        createInfo.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
        createInfo.viewType = VK_IMAGE_VIEW_TYPE_2D;
        createInfo.format = m_surfaceFormat.format;
        createInfo.components.r = VK_COMPONENT_SWIZZLE_IDENTITY;
        createInfo.components.g = VK_COMPONENT_SWIZZLE_IDENTITY;
        createInfo.components.b = VK_COMPONENT_SWIZZLE_IDENTITY;
        createInfo.components.a = VK_COMPONENT_SWIZZLE_IDENTITY;
        createInfo.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
        createInfo.subresourceRange.baseMipLevel = 0;
        createInfo.subresourceRange.levelCount = 1;
        createInfo.subresourceRange.baseArrayLayer = 0;
        createInfo.subresourceRange.layerCount = 1;

        VkDevice logicalDevice = m_device.GetLogicDevice();

        for (auto i = 0; i < m_swapchainImageCount; i++) {
            createInfo.image = m_swapchainImages[i];

            if (vkCreateImageView(logicalDevice, &createInfo, nullptr, &m_swapchainImageViews[i]) != VK_SUCCESS) {
                CORE_ERROR("[Vulkan Error] Image view creation failed!");
            }

            m_device.SetDebugMarkerName(m_swapchainImageViews[i], VK_DEBUG_REPORT_OBJECT_TYPE_IMAGE_VIEW_EXT, m_debugName);
        }
    }

    VkSurfaceTransformFlagBitsKHR Swapchain::GetSurfaceCapabilitiesData(uint32_t& windowWidth, uint32_t& windowHeight){
        VkSurfaceCapabilitiesKHR surfaceCapabilities = {};

        if (vkGetPhysicalDeviceSurfaceCapabilitiesKHR(m_device.GetPhysicalDevice(), m_surface, &surfaceCapabilities) != VK_SUCCESS) {
            CORE_ERROR("[Vulkan Error] vkGetPhysicalDeviceSurfaceCapabilitiesKHR failed!");
        }

        //Get the valid extent
        if (surfaceCapabilities.currentExtent.width == std::numeric_limits<uint32_t>::max() &&
            surfaceCapabilities.currentExtent.height == std::numeric_limits<uint32_t>::max()) {
            m_extent.width = windowWidth;
            m_extent.height = windowHeight;
        } else {
            m_extent = surfaceCapabilities.currentExtent;
            windowWidth = surfaceCapabilities.currentExtent.width;
            windowHeight = surfaceCapabilities.currentExtent.height;
        }

        //Set image count
        uint32_t numOfImages = surfaceCapabilities.minImageCount +1;
        if(surfaceCapabilities.maxImageCount < numOfImages){
            numOfImages = surfaceCapabilities.maxImageCount;
        }

        m_swapchainImageCount = numOfImages;

        //Return the valid transform
        VkSurfaceTransformFlagBitsKHR transform = {};
        if (surfaceCapabilities.supportedTransforms & VK_SURFACE_TRANSFORM_IDENTITY_BIT_KHR) {
        //Non-rotated transform is prefered.
            transform = VK_SURFACE_TRANSFORM_IDENTITY_BIT_KHR;
        } else {
            transform = surfaceCapabilities.currentTransform;
        }
        return transform;
    }
    VkPresentModeKHR Swapchain::ChoosePresentationMode(){
        if(m_vsync){
            //Guaranteed to be on every device
             CORE_INFO("[Vulkan Info] Present Mode FIFO_KHR was chosen.");
            return VK_PRESENT_MODE_FIFO_KHR;
        }

        uint32_t presentModeCount;
        
        if(vkGetPhysicalDeviceSurfacePresentModesKHR(m_device.GetPhysicalDevice(),m_surface, &presentModeCount, nullptr) != VK_SUCCESS){
            CORE_ERROR("[Vulkan Error] No present modes could be found on the device!");
        }
        if(presentModeCount == 0){
            CORE_ERROR("[Vulkan Error] No present modes could be found on the device!");
        }

        std::vector<VkPresentModeKHR> presentModes(presentModeCount);
        if(vkGetPhysicalDeviceSurfacePresentModesKHR(m_device.GetPhysicalDevice(), m_surface, &presentModeCount, presentModes.data())!= VK_SUCCESS){
            CORE_ERROR("[Vulkan Error] Failed to get present modes on the device!");
        }
        //Present modes differ a lot from one another. MAILBOX_KHR has highest priority, but if it isn't available, other modes will be chosen.
        //Try to get MAILBOX_KHR
        for(auto i : presentModes){
         /*VK_PRESENT_MODE_MAILBOX_KHR specifies that the presentation engine waits for the next vertical blanking
         period to update the current image. Tearing cannot be observed. An internal single-entry queue is used to
         hold pending presentation requests. If the queue is full when a new presentation request is received, the new
         request replaces the existing entry, and any images associated with the prior entry become available for
         re-use by the application. One request is removed from the queue and processed during each vertical blanking
         period in which the queue is non-empty.*/
            if(i == VK_PRESENT_MODE_MAILBOX_KHR){
                 CORE_INFO("[Vulkan Info] Present Mode MAILBOX_KHR was chosen.");
                 return i;
            }
        }
        CORE_WARN("[Vulkan Warning] Present mode MAILBOX_KHR was unavailable");

        //Try to get 
        for (auto i : presentModes) {
        /*VK_PRESENT_MODE_IMMEDIATE_KHR specifies that the presentation engine does not wait for a vertical blanking
        period to update the current image, meaning this mode may result in visible tearing. No internal queuing of
        presentation requests is needed, as the requests are applied immediately.*/
            if (i == VK_PRESENT_MODE_IMMEDIATE_KHR) {
                CORE_INFO("[Vulkan Info] Present Mode IMMEDIATE_KHR was chosen.");
                return i;
            }
        }
        CORE_WARN("[Vulkan Warning] Present mode IMMEDIATE_KHR was unavailable");

        for(auto i : presentModes){
        /*VK_PRESENT_MODE_FIFO_KHR specifies that the presentation engine waits for the next vertical blanking period
        to update the current image. Tearing cannot be observed. An internal queue is used to hold pending
        presentation requests. New requests are appended to the end of the queue, and one request is removed from the
        beginning of the queue and processed during each vertical blanking period in which the queue is non-empty.
        This is the only value of presentMode that is required to be supported.*/
            if(i == VK_PRESENT_MODE_FIFO_KHR){
                CORE_INFO("[Vulkan Info] Present Mode FIFO_KHR was chosen.");
                return i;
            }
        }
        CORE_ERROR("[Vulkan Error] Somehow not even present mode FIFO_KHR was found! No Present modes found!");
         return VK_PRESENT_MODE_FIFO_KHR;
    }
    void Swapchain::ChooseSurfaceColorFormat(){
        VkPhysicalDevice graphicsCard = m_device.GetPhysicalDevice();

        std::uint32_t numOfAvailableFormats = 0;

        if (vkGetPhysicalDeviceSurfaceFormatsKHR(graphicsCard, m_surface, &numOfAvailableFormats, nullptr) != VK_SUCCESS) {
            CORE_ERROR("[Vulkan Error] vkGetPhysicalDeviceSurfaceFormatsKHR failed!");
        }

        if (numOfAvailableFormats == 0) {
            CORE_ERROR("[Vulkan Error] No surface formats could be found!");
        }

        std::vector<VkSurfaceFormatKHR> availableFormats(numOfAvailableFormats);

        // Get information about all surface formats available.
        if (vkGetPhysicalDeviceSurfaceFormatsKHR(graphicsCard, m_surface, &numOfAvailableFormats, availableFormats.data()) != VK_SUCCESS) {
            CORE_ERROR("[Vulkan Error] vkGetPhysicalDeviceSurfaceFormatsKHR failed!");
        }


        for (auto i : availableFormats) {
            //VK_FORMAT_R8G8B8A8_SRGB
            if (i.format == VK_FORMAT_B8G8R8A8_SRGB && i.colorSpace == VK_COLOR_SPACE_SRGB_NONLINEAR_KHR) {
                m_surfaceFormat = i;
                return;
            }
	    }
        CORE_ERROR("[Vulkan Error] No valid surface formats found!");
    }

 }
