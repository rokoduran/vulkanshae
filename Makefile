# I wonder what these two are?
mkfile_path := $(abspath $(lastword $(MAKEFILE_LIST)))
current_dir := $(notdir $(patsubst %/,%,$(dir $(mkfile_path))))

# Feel free to substitute 'g++' with 'clang++'!
CXX=g++

# Tune tune to your heart's content
#-g = debug info -O2 = release optimization mode -pedantic
CPPFLAGS=-std=c++17 -o2 -DDEBUG 
CPPWFLAGS=#-Wall -Wextra  Fuck you, listen to the warnings


# This will be your executable
BUILD_TARGET=Shae
BUILD_FORMAT=exe

SDIR=src
HDIR=$(SDIR)/header
LDIR=$(SDIR)/lib

BDIR=bin
ODIR=$(BDIR)/obj

# Optionally put 3rd party libraries in "src/header/libraries/"?
# Or use some folder that's not even in the project structure
# if those include files are used accross multiple projects
INC=-I$(HDIR) -I$(HDIR)/libraries

# Delicious filesystem library. fs went at end of lstdc++
LDFLAGS=-lstdc++fs
LIBS=-L$(LDIR) $(LDFLAGS)

# This is magic
VPATH=$(SDIR):$(OBJDIR):$(LDIR)

vpath %.h $(HDIR)
vpath %.o $(ODIR)
vpath %.a $(LDIR)

INCLUDE=$(INC)
LIBRARIES=$(LIBS)
#new
LIBRARIES += -l:libglfw3.a -l:vulkan-1.a -lgdi32

# Cool!
OBJECTS := $(wildcard $(ODIR)/*.o)

SRC=$(wildcard $(SDIR)/*.cpp)
SRC_NAMES=$(subst $(SDIR)/,,$(SRC))
OBJ_INTERMEDIATE=$(subst $(SDIR),$(ODIR),$(SRC))
OBJ=$(OBJ_INTERMEDIATE:.cpp=.o)
OBJ_NAMES=$(SRC_NAMES:.cpp=.o)

# This thing builds your executable. It's the default task
# that 'make' will try to accomplish
target: $(OBJ_NAMES)
	$(CXX) $(OBJ) -static $(LIBRARIES) -o $(BDIR)/$(BUILD_TARGET).$(BUILD_FORMAT)
	@echo "Building done"

# This thing builds for all .cpp files
%.o: %.cpp
	$(CXX) $(CPPFLAGS) $(CPPWFLAGS) $(INCLUDE) $< -c -o $(ODIR)/$@

# 'target' and '%.o' will be called automatically! Wow!

# phony targets are not actual 'build' targets, they just
# do some non-compile/build tasks
.PHONY: debug
.PHONY: clean
.PHONY: v
.PHONY: rebuild



debug:
	$(CPPFLAGS)-DDEBUG
	@echo "Running debug mode for compiler" $(CXX);
	@make

#	CPPFLAGS += -DDEBUG
#	@make -j6

# Target to remove .o files in 'bin/obj'
# Used by the 'rebuild' target
clean:
	@for obj in $(OBJECTS) ; do \
    echo "Removing" $$obj ; \
		rm $$obj ; \
    done
	@echo "Cleanup done"

v:
	$(CXX) -v

rebuild:
	@echo "Removing existing obj files..."
	@make clean
	@echo "Building the project..."
	@make -j6
